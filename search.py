from PIL import ImageTk
import PIL.Image
from tkinter import *
import tkinter as tk
import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel

windo = Tk()
windo.configure(background='white')
windo.title("Recommended App")
windo.geometry('1120x720')
windo.resizable(0,0)

def clear():
    txt2.delete(first=0,last=100)
    T.destroy()
    L.destroy()
    M.destroy()
    FA1.destroy()

def search():
    try:
        global result,T,L, FA1, M
        query = txt2.get()

        movies = pd.read_csv('./asset/movies.csv', sep=',', encoding='latin-1', usecols=['movieId','title','genres'])
        tfidf_movies_genres = TfidfVectorizer(token_pattern = '[a-zA-Z0-9\-]+')
        movies['genres'] = movies['genres'].replace(to_replace="(no genres)", value="")
        tfidf_movies_genres_matrix = tfidf_movies_genres.fit_transform(movies['genres'])
        cosine_sim_movies = linear_kernel(tfidf_movies_genres_matrix, tfidf_movies_genres_matrix)
        def get_recommendations_based_on_genres(movie_title, cosine_sim_movies=cosine_sim_movies):
            idx_movie = movies.loc[movies['title'].isin([movie_title])]
            idx_movie = idx_movie.index
            sim_scores_movies = list(enumerate(cosine_sim_movies[idx_movie][0]))
            sim_scores_movies = sorted(sim_scores_movies, key=lambda x: x[1], reverse=True)
            sim_scores_movies = sim_scores_movies[1:3]
            movie_indices = [i[0] for i in sim_scores_movies]
            return movies['title'].iloc[movie_indices]
 
        result = get_recommendations_based_on_genres(query)
        result = result.values.tolist()
        M = tk.Label(windo, text="Film Recommendation", width=20, height=1,font=('times', 16, ' bold'))
        M.place(x=430, y=210)

        T = tk.Text(windo, borderwidth=-1, height=1, width=len(result[0]), font=('times', 16))
        T.pack()
        T.place(x=430, y=260)
        T.insert(tk.END, result[0])

        L = tk.Text(windo, borderwidth=-1, height=1, width=len(result[1]), font=('times', 16))
        L.place(x=430, y=310)
        L.insert(tk.END, result[1])

        FA1 = tk.Button(windo, text="Clear",command = clear, fg="white", bg="red", font=('times', 15, ' bold '))
        FA1.place(x=430, y=360)
    except Exception as e:
        lab2 = tk.Label(windo, text="Not Found", width=20, height=1, fg="white", bg="red",
                        font=('times', 16, ' bold '))
        lab2.place(x=720, y=605)
        windo.after(5000, destroy_widget, lab2)

def destroy_widget(widget):
    widget.destroy()

im = PIL.Image.open('./asset/film.png')
im =im.resize((300,250), PIL.Image.ANTIALIAS)
wp_img = ImageTk.PhotoImage(im)
panel4 = Label(windo, image=wp_img,bg = 'white')
panel4.pack()
panel4.place(x=20, y=80)

im1 = PIL.Image.open('./asset/search.png')
im1 =im1.resize((70,70), PIL.Image.ANTIALIAS)
sp_img = ImageTk.PhotoImage(im1)
panel5 = Button(windo,borderwidth=0,command = search, image=sp_img,bg = 'white')
panel5.pack()
panel5.place(x=920, y=120)

pred = tk.Label(windo, text="Film Recommendation APP", width=30, height=1, fg="white",bg="black",
                font=('times', 20, ' bold '))
pred.place(x=350, y=10)

lab = tk.Label(windo, text="Input Movie To Get Recommendation", width=32, height=1, fg="black",
                font=('times', 16, ' bold '))
lab.place(x=510, y=80)

txt2 = tk.Entry(windo,borderwidth = 7, width=26, bg="white", fg="black", font=('times', 25, ' bold '))
txt2.place(x=430, y=130)

P = tk.Label(windo, text="Movie List", width=20, height=1,font=('times', 16, ' bold'))
P.place(x=50, y=400)

film1 = PIL.Image.open('./asset/Casino.jpg')
film1 =film1.resize((150,200), PIL.Image.ANTIALIAS)
film1 = ImageTk.PhotoImage(film1)
panel6 = Label(windo, image=film1,bg = 'white')
panel6.pack()
panel6.place(x=50, y=450)
f1 = tk.Label(windo, text="Casino (1995)", width=15, height=1,font=('times', 14, ' bold'))
f1.place(x=55, y=665)

film2 = PIL.Image.open('./asset/balto.jpg')
film2 =film2.resize((150,200), PIL.Image.ANTIALIAS)
film2 = ImageTk.PhotoImage(film2)
panel7 = Label(windo, image=film2,bg = 'white')
panel7.pack()
panel7.place(x=270, y=450)
f2 = tk.Label(windo, text="Balto (1995)", width=15, height=1,font=('times', 14, ' bold'))
f2.place(x=275, y=665)

film3 = PIL.Image.open('./asset/Heat.jpg')
film3 =film3.resize((150,200), PIL.Image.ANTIALIAS)
film3 = ImageTk.PhotoImage(film3)
panel8 = Label(windo, image=film3,bg = 'white')
panel8.pack()
panel8.place(x=490, y=450)
f2 = tk.Label(windo, text="Heat (1995)", width=15, height=1,font=('times', 14, ' bold'))
f2.place(x=495, y=665)

film4 = PIL.Image.open('./asset/Jumanji.jpg')
film4 =film4.resize((150,200), PIL.Image.ANTIALIAS)
film4 = ImageTk.PhotoImage(film4)
panel9 = Label(windo, image=film4,bg = 'white')
panel9.pack()
panel9.place(x=710, y=450)
f2 = tk.Label(windo, text="Jumanji (1995)", width=15, height=1,font=('times', 14, ' bold'))
f2.place(x=715, y=665)

film5 = PIL.Image.open('./asset/Sabrina.jpg')
film5 =film5.resize((150,200), PIL.Image.ANTIALIAS)
film5 = ImageTk.PhotoImage(film5)
panel9 = Label(windo, image=film5,bg = 'white')
panel9.pack()
panel9.place(x=930, y=450)
f2 = tk.Label(windo, text="Sabrina (1995)", width=15, height=1,font=('times', 14, ' bold'))
f2.place(x=935, y=665)

windo.mainloop()
